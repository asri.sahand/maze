# depth-limited dfs
from priodict import priorityDictionary
def dls_solver(maze, limit):
	path = [maze.start]
	visited = set(maze.start)
	if dls_dfs(maze, path, visited, maze.start, limit):
		return True , path
	return False , path

def iterative_dfs_solver(maze):
	path = []
	for i in range(1,10000):
		path = []
		flag , path = dls_solver(maze, i)
		if flag:
			return path
	return path


def dfs_solver(maze):
	path = [maze.start]
	visited = set(maze.start)
	dfs(maze, path, visited, maze.start)
	return path

def dfs(maze, path, visited, cell): 
	if cell == maze.goal:
		return True
	for nei in maze.get_neighbors(cell):
		if nei not in visited:
			visited.add(nei)
			path.append(nei)
			if dfs(maze, path, visited, nei):
				return True


def dls_dfs(maze, path, visited, cell, limit): 
	if limit == 0:
		return False
	if cell == maze.goal:
		return True
	for nei in maze.get_neighbors(cell):
		if nei not in visited:
			visited.add(nei)
			path.append(nei)
			if dls_dfs(maze, path, visited, nei, limit-1):
				return True


def bfs_solver(maze):
	path = [maze.start]
	queue = [maze.start]
	visited = set()
	while queue:
		cell = queue.pop()
		if(cell == maze.goal):
			path.append(maze.goal)
			break
		elif cell not in visited:
			visited.add(cell)
			path.append(cell)
			for nei in maze.get_neighbors(cell):
				if(nei not in visited):
					queue.insert(0, nei)
					path.append(nei)
	return path

def astar_heuristic(maze, cell):
	return ( (cell[0]-maze.goal[0])**2 + (cell[1]-maze.goal[1])**2 )



def usc(maze):
	dist = {}	# dictionary of final distances
	parent = {}	# dictionary of predecessors
	queue = priorityDictionary()   # est.dist. of non-final vert.
	queue[maze.start] = 0
	
	for cell in queue:
		dist[cell] = queue[cell]
		if cell == maze.goal: 
			break
		for nei in maze.get_neighbors(cell):
			cost = dist[cell] + astar_heuristic(maze, nei)
			if nei not in dist and (nei not in queue or cost < queue[nei]):
				queue[nei] = cost
				parent[nei] = cell
	return parent
			
def astar_solver(maze):

	parent = usc(maze)
	Path = []
	end = maze.goal
	while 1:
		Path.append(end)
		if end == maze.start: break
		end = parent[end]
	Path.reverse()
	return Path
